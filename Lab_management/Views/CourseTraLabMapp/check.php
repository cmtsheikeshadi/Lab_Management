<?php

include_once ('../../vendor/autoload.php');
use App\CourseTraLabMapp\CourseTraLabMapp;

$obj = new CourseTraLabMapp();

if(isset($_POST['course_id'])){
    $id['id']=$_POST['course_id'];
 //  print_r($id); 
    $data = $obj->prepare($id)->courses_show();
//echo '<pre>';    
  //  print_r($data);
    $course_name['course_name'] = $data['title'];
    $trainer_list = $obj->prepare($course_name)->trainers_show();
//echo '<br/><br/><br/>';    
  
   $lab_no = $obj->prepare($id)->labinfo_show(); 
   
   //print_r($lab_no);
 }

 
 ?>
        <span><div class="form-group">
                       <h4> Select Trainer </h4>    
                       <label> &nbsp; &nbsp;* Lead Trainer:</label>
                            <select name='lead_trainer' required="required">
                                        <option value="">Select Lead Trainer Name</option>
                            <?php if(isset($trainer_list)){?>
                                <?php foreach($trainer_list as $list):?>
                                    <?php if(isset($list['trainer_level']) && $list['trainer_level']=='Led Trainer'){?>
                                        <option value='<?php echo $list['full_name'];?>'> <?php echo $list['full_name'];?></option>
                                    <?php }?>
                                <?php endforeach;}?>
                            </select>

                       <label> &nbsp; &nbsp;* Assistant Trainer:</label>
                       <select name='asst_trainer' required="required">
                                        <option value="">Select Assistant Trainer Name</option>
                                <?php if(isset($trainer_list)){?>
                                <?php foreach($trainer_list as $list):?>
                                    <?php if(isset($list['trainer_level']) && $list['trainer_level']=='Assistant Trainer'){?>
                                        <option value='<?php echo $list['full_name'];?>'> <?php echo $list['full_name'];?></option>
                                    <?php }?>
                                <?php endforeach;}?>
                        </select>
                       
                            <label> &nbsp; &nbsp; Lab Trainer:</label>
                              <select name='lab_asst'>
                                    <option value="">Select Lab Assistant Name</option>
                            <?php if(isset($trainer_list)){?>
                                <?php foreach($trainer_list as $list):?>
                                    <?php if(isset($list['trainer_level']) && $list['trainer_level']=='Lab Assistant'){?>
                                        <option value='<?php echo $list['full_name'];?>'> <?php echo $list['full_name'];?></option>
                                    <?php }?>
                                <?php endforeach;}else{echo 'Not teacher';}?>
                    </select>
        </div>
        <div class="form-group">
                  <label> &nbsp; &nbsp; Lab No:</label>
                  <select name="lab_id" required="required">
                      <option value="">Select Lab No</option>
                        <?php if(isset($lab_no)){?>
                                <?php foreach($lab_no as $lab_list):?>
                                        <option value="<?php echo $lab_list['lab_no'];?>"> <?php echo $lab_list['lab_no'];?> </option>
                                <?php endforeach;}?>
                  </select>
        </div>
        
        </span>
