<?php
include_once "links.php";
echo "<body>";
include_once "header.php";

//<!-- Page container -->
echo '<div class="page-container">

		<!-- Page content -->
		<div class="page-content">';

include_once "menu.php";
?>

    
<?php include_once ('../../vendor/autoload.php'); ?>
<?php
include_once ('../../vendor/autoload.php');
use App\Courses\Courses;
$obj = new Courses();
$data = $obj->index();
//print_r($data);
?>


<div class="content-wrapper">

    <!-- Page header -->
<?php include 'pageheader.php';?>
    <!-- /page header -->

    <!-- Content area -->
    <div class="content">


        <!-- Multiple rows select -->
        <div class="panel panel-flat">
            <div class="panel-heading">
                <h5 class="panel-title">Course Lists</h5>
                <div class="heading-elements">
                    <ul class="icons-list">
                        <li><a data-action="collapse"></a></li>
                        <li><a data-action="reload"></a></li>
                        <li><a data-action="close"></a></li>
                    </ul>
                </div>
            </div>
            <form action="store.php" method="POST">
                <div class="form-group">
                    <label> &nbsp; &nbsp; Course Title:</label>
                    <select id="select" name="course_id" required="required" >
                                                <option value="">Select Course Name</option>
                                            <?php foreach ($data as $item):?>
                                                <option value="<?php echo $item['id'];?>"> <?php echo $item['title'];?></option>
                                            <?php endforeach;?>
                        </select>
                </div>

                <div class="form-group">
                    <span id="availability2"></span>
                </div>
                
                <div class="form-group">
                    <label> &nbsp; &nbsp; Batch NO:</label>
                    <select name="batch_no">
                                    <option value=""> Select Batch No</option>
                                <?php for($i=1; $i<=100; $i++): ?>
                                    <option value="<?php echo $i;?>"><?php echo $i;?></option>
                                <?php endfor;?>       
                    </select>
                </div>
                
                <div class="form-group">
                    <label> &nbsp; &nbsp; Start Data:</label>
                        <input type="date" name="start_date" value="">
                    <label> &nbsp; &nbsp; Ending Data:</label>
                        <input type="date" name="ending_date" value="">
                </div>
               
                <div class="form-group">
                     <label> &nbsp; &nbsp; Start Time:</label>
                      <select id="valid" name="start_time">
                                    <option value="">Select Start Time</option>
                                    <option value="9.00 am">9.00 am</option>
                                    <option value="1.30 pm">1.30 pm</option>
                                    <option value="5.30 pm">5.30 pm</option>
                                    <option value="9.30 pm">9.30 pm</option>
                      </select>
                     <label> &nbsp; &nbsp; Ending Time:</label>
                     <select name="ending_time">
                            <option value="">Select Ending Time</option>
                            <option value="9.00 am">9.00 am</option>
                            <option value="1.30 pm">1.30 pm</option>
                            <option value="5.30 pm">5.30 pm</option>
                            <option value="9.30 pm">9.30 pm</option>
                       </select>
                </div>
                
                <div class="form-group">
                    <label> &nbsp; &nbsp; Day:</label>
                            <select name="day">
                                <option value=""> Select Day</option>
                                <option value="Friday">Friday</option>
                                <option value="Satur-Mon-Wednes">Satur-Mon-Wednes</option>
                                <option value="Sun-Tues-Thurs">Sun-Tues-Thurs</option>
                            </select>
                </div>
                
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-2"></div>
                        <div class="col-md-8">
                            <input type="submit" class="btn btn-info btn-block center" name="submit" value="Submit Button">
                        </div>
                        <div class="col-md-2"></div>
                    </div>
              </div>
           </form>
 <script src="jquery-3.1.0.min.js" type="application/javascript"></script>

<script>
    $(document).ready(function(){
        $('#select').click(function(){
            var course_id = $(this).val();
            $.ajax({
                url:"check.php",
                method:"POST",
                data:{course_id:course_id},
                dataType:"text",
                success:function(html)
                {
                    $('#availability2').html(html);
                }
            });
        });
    });
</script>

        
        </div>
    
 
            
        </div>

        <!-- /multiple rows select -->


    &copy; 2015. <a href="#">Limitless Web App Kit</a> by <a href="http://themeforest.net/user/Kopyov" target="_blank">Eugene Kopyov</a>
    </div>
<!-- Footer -->



<!-- /footer -->

<?php echo '</div>

		<!-- Page content -->
		</div> </body> </html>';
?>
