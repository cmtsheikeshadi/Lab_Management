<?php
include_once ('../../vendor/autoload.php');
use App\Courses\Courses;

$obj = new Courses();
$data = $obj->index();
/*echo $data[0]['title'];
die();*/
?>
<?php
/**
 * Created by PhpStorm.
 * User: Mahbub-Alam
 * Date: 08-08-16
 * Time: 12.03
 */
include_once "links.php";
?>
<style>
.button {
background-color:#4CAF50 ; /* Green */
border: none;
color: white;
padding: 0 32px;
text-align: center;
text-decoration: none;
display: inline-block;
font-size: 16px;
margin: 2px 1px;
-webkit-transition-duration: 0.4s; /* Safari */
transition-duration: 0.4s;
cursor: pointer;
}
.button2 {
    background-color: #008CBA;
    color: white;
    border: 2px solid #008CBA;
}
.button2:hover {
    background-color:white ;
    color: black;
}
</style>


<?php
echo "<body>";
include_once "header.php";

//<!-- Page container -->
echo '<div class="page-container">

		<!-- Page content -->
		<div class="page-content">';

?>
<?php include 'menu.php';?>
    <!-- Page header -->
<?php include 'pageheader.php';?>    
    <!-- /page header -->


    <!-- Content area -->
    <div class="content">


        <!-- Multiple rows select -->
        <div class="panel panel-flat">
            <div class="panel-heading">
                <h5 class="panel-title"> Lists</h5>
                
                <select class="classic" id="search">
                    <option value="">All</option>
             <?php foreach ($data as $items):?>
                    <option value="<?php echo $items['title'];?>"><?php echo $items['title'];?></option>
             <?php endforeach;?>
                </select>

                <div class="heading-elements">
                    <ul class="icons-list">
                        <li><a data-action="collapse"></a></li>
                        <li><a data-action="reload"></a></li>
                        <li><a data-action="close"></a></li>
                    </ul>
                </div>
            </div>

       <span id="availability2">
            <table class="table datatable-tools-select-multiple" id="dt">
                <thead>
                <tr>
                    <th>Title</th>
                    <th>Course Type</th>
                    <th>Course Fee</th>
                    <th>Description</th>
                    <th>Duration</th>
                    <th>Is Active</th>
                    <th class="text-center">Actions</th>
                </tr>
                </thead>
                <tbody>
                <?php if(isset($data)){?>
                    <?php  foreach ($data as $item):?>
                        <?php $duration = unserialize($item['duration']);?>

                    <tr>
                        <td><?php echo $item['title']; ?></td>
                        <td><?php echo $item['course_type']; ?></td>
                        <td><?php echo $item['course_fee']; ?></td>
                        <td><?php echo $item['description']; ?></td>
                        <td><?php echo $duration['month']." ".$duration['hours']; ?></td>
                        <td><?php echo $item['is_active']; ?></td>
                        <td>
                            <ul class="icons-list">
                                <li class="text-primary-600"><a href="edit.php?id=<?php echo $item['unique_id']; ?>"><i class="icon-pencil7"></i></a></li>
                                <li class="text-danger-600"><a href="delete.php?id=<?php echo $item['unique_id']; ?>"><i class="icon-trash"></i></a></li>
                                <li class="text-teal-600"><a href="show.php?id=<?php echo $item['unique_id']; ?>"><i class="icon-three-bars"></i></a></li>
                                <li class="text-primary-600"><a href="#"><i class="icon-detail"></i></a></li>
                            </ul>
                            
                        </td>
                    </tr>
                    <?php endforeach;?>
                <?php }else { ?>
                    <tr>
                        <td colspan="6"> <p style='color:red;'> No Data</p></td>
                    </tr>
                <?php }?>
                </tbody>
            </table>
</span>
        </div>
        <!-- /multiple rows select -->



    



<!-- Footer -->
<div class="footer text-muted">
    &copy; 2015. <a href="#">Limitless Web App Kit</a> by <a href="http://themeforest.net/user/Kopyov" target="_blank">Eugene Kopyov</a>
</div>
</div>
<!-- /footer -->

<?php echo '</div>

		<!-- Page content -->
		</div> </body> </html>';
?>
