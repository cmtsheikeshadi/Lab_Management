<?php include_once ('../../vendor/autoload.php');

include_once ('../../vendor/autoload.php');
use App\Courses\Courses;

$obj = new Courses();
$data = $obj->prepare($_GET)->show();
$duration = unserialize($data['duration']);

?>


<?php
include_once "links.php";
echo "<body>";
include_once "header.php";

//<!-- Page container -->
echo '<div class="page-container">

		<!-- Page content -->
		<div class="page-content">';

include_once "menu.php";
?>
    <div class="content-wrapper">

        <!-- Page header -->
        <div class="page-header">
            <div class="page-header-content">
                <div class="page-title">
                    <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Home</span> - Course Setup</h4>
                </div>

                <div class="heading-elements">
                    <div class="heading-btn-group">
                        <a href="#" class="btn btn-link btn-float has-text"><i class="icon-bars-alt text-primary"></i><span>Statistics</span></a>
                        <a href="#" class="btn btn-link btn-float has-text"><i class="icon-calculator text-primary"></i> <span>Invoices</span></a>
                        <a href="#" class="btn btn-link btn-float has-text"><i class="icon-calendar5 text-primary"></i> <span>Schedule</span></a>
                    </div>
                </div>
            </div>

            <div class="breadcrumb-line">
                <ul class="breadcrumb">
                    <li><a href="index.html"><i class="icon-home2 position-left"></i> Home</a></li>
                    <li class="active">Course Setup</li>
                </ul>

                <ul class="breadcrumb-elements">
                    <li><a href="#"><i class="icon-comment-discussion position-left"></i> Support</a></li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <i class="icon-gear position-left"></i>
                            Settings
                            <span class="caret"></span>
                        </a>

                        <ul class="dropdown-menu dropdown-menu-right">
                            <li><a href="#"><i class="icon-user-lock"></i> Account security</a></li>
                            <li><a href="#"><i class="icon-statistics"></i> Analytics</a></li>
                            <li><a href="#"><i class="icon-accessibility"></i> Accessibility</a></li>
                            <li class="divider"></li>
                            <li><a href="#"><i class="icon-gear"></i> All settings</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
        <!-- /page header -->


        <!-- Content area -->
        <div class="content">

            <div class="row">
                <div class="col-md-16">

                    <!-- Basic layout-->
                    <form action="update.php" method="POST">
                        <div class="panel panel-flat">
                            <div class="panel-heading">
                                <h5 class="panel-title">Course SetUp</h5>
                                <div class="heading-elements">
                                    <ul class="icons-list">
                                        <li><a data-action="collapse"></a></li>
                                        <li><a data-action="reload"></a></li>
                                        <li><a data-action="close"></a></li>
                                    </ul>
                                </div>
                            </div>

                            <div class="panel-body">
                                <div class="form-group">
                                    <label>Course Title:</label>
                                    <input type="text" class="form-control" name="title" required="required" value="<?php echo $data['title'];?>" placeholder="Course Title">
                                </div>

                                <div class="form-group">
                                    <label>Course Type:</label>
                                    <select name="course_type" id="select" class="select form-control">
                                        <option value="Free Course"> Free Course </option>
                                        <option value="Paid Course"> Paid Course </option>
                                    </select>
                                </div>

                                <div class="form-group">

                                    <span id="availability2"></span>


                                </div>

                                <div class="form-group">
                                    <label>Course Duration In Month:</label>
                                    <select name="duration_m" class="select form-control">
                                        <option value="<?php echo $duration['month'];?>"><?php echo $duration['month'];?> </option>>
                                        <option value="2 Month"> 2 Month</option>
                                        <option value="3 Month"> 3 month</option>
                                        <option value="4 Month"> 4 Month</option>
                                        <option value="5 Month"> 5 month</option>
                                        <option value="6 Month"> 6 Month</option>
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label>Course Duration In Houre:</label>
                                    <select name="duration_h" class="select form-control">
                                        <option value="<?php echo $duration['hours'];?>"><?php echo $duration['hours'];?> </option>>
                                        <option value="144 Hours"> 144 Hours</option>
                                        <option value="128 Hours"> 128 Hours</option>
                                        <option value="48 Hours"> 48 Hours</option>
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label class="text-semibold">Is Active?</label>
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" name="is_active" checked="checked">
                                            Checked default
                                        </label>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label>Description</label>

                                    <textarea cols="10" rows="6" name="description" class="wysihtml5 wysihtml5-default form-control" placeholder="Enter text ...">

                                    <?php echo $data['description'];?>
                                    </textarea>
                                </div>

<input type="hidden" name="id" value="<?php echo $data['unique_id'];?>">
                            </div>

                            <div class="row">
                                <div class="col-md-2"></div>
                                <div class="col-md-8">
                                    <input type="submit" class="btn btn-info btn-block center" name="submit" value="Submit Button">
                                </div>
                                <div class="col-md-2"></div>
                            </div>


                        </div>
                </div>
            </div>
            </form>
            <!-- /basic layout -->
            
        </div>

    </div>

    <!-- Footer -->
    <div class="footer text-muted">
        &copy; 2015. <a href="#">Limitless Web App Kit</a> by <a href="http://themeforest.net/user/Kopyov" target="_blank">Eugene Kopyov</a>
    </div>
    <!-- /footer -->

    </div>
    <!-- /content area -->

    </div>
<?php echo '</div>

		<!-- Page content -->
		</div> </body> </html>';
?>