<?php

include_once ('../../vendor/autoload.php');
use App\Courses\Courses;

$obj = new Courses();
$data = $obj->trashed_show();
       
//print_r($data);

    if(isset($_SESSION['Message']) && !empty($_SESSION['Message'])){
        echo $_SESSION['Message'];
        unset($_SESSION['Message']);
    }
?>


<?php
/**
 * Created by PhpStorm.
 * User: Mahbub-Alam
 * Date: 08-08-16
 * Time: 12.03
 */
include_once "links.php";
echo "<body>";
include_once "header.php";

//<!-- Page container -->
echo '<div class="page-container">

		<!-- Page content -->
		<div class="page-content">';

include_once "menu.php";
?>
<div class="content-wrapper">
<?php include 'pageheader.php';?>
 <!-- Content area -->
    <div class="content">


        <!-- Multiple rows select -->
        <div class="panel panel-flat">
            <div class="panel-heading">
                <h5 class="panel-title">Delete Course Lists</h5>
              
                <select class="classic" id="search">
                    <option value="">All</option>
             <?php foreach ($data as $items):?>
                    <option value="<?php echo $items['title'];?>"><?php echo $items['title'];?></option>
             <?php endforeach;?>
                </select>

                <div class="heading-elements">
                    <ul class="icons-list">
                        <li><a data-action="collapse"></a></li>
                        <li><a data-action="reload"></a></li>
                        <li><a data-action="close"></a></li>
                    </ul>
                </div>
            </div>

       <span id="availability2">
            <table class="table datatable-tools-select-multiple" id="dt">
                <thead>
                <tr>
                    <th>Title</th>
                    <th>Course Type</th>
                    <th>Course Fee</th>
                    <th>Description</th>
                    <th>Duration</th>
                    <th>Is Active</th>
                    <th class="text-center">Actions</th>
                </tr>
                </thead>
                <tbody>
                <?php if(isset($data)){?>
                    <?php  foreach ($data as $item):?>
                        <?php $duration = unserialize($item['duration']);?>

                    <tr>
                        <td><?php echo $item['title']; ?></td>
                        <td><?php echo $item['course_type']; ?></td>
                        <td><?php echo $item['course_fee']; ?></td>
                        <td><?php echo $item['description']; ?></td>
                        <td><?php echo $duration['month']." ".$duration['hours']; ?></td>
                        <td><?php echo $item['is_active']; ?></td>
                        <td>
                            <ul class="icons-list">
                                <li class="text-danger-600"><a href="restore.php?id=<?php echo $item['unique_id']; ?>" onclick="return confirm('Are you sure you want to Restore this item?');">Restore</a></li>
                                <li class="text-teal-600"><a href="delete.php?id=<?php echo $item['unique_id']; ?>" onclick="return confirm('Are you sure you want to Delete this item?');"><i class="icon-trash"></i></a></li>
                                
                            </ul>
                            
                        </td>
                    </tr>
                    <?php endforeach;?>
                <?php }else { ?>
                    <tr>
                        <td colspan="6"> <p style='color:red;'> No Data</p></td>
                    </tr>
                <?php }?>
                </tbody>
            </table>
</span>
        </div>
        <!-- /multiple rows select -->



    </div>
<?php echo '</div>

		<!-- Page content -->
		</div> </body> </html>';
?>
