<?php

include_once ('../../vendor/autoload.php');
use App\Courses\Courses;

$obj = new Courses();
$data = $obj->prepare($_GET)->show();
?>
<?php $duration = unserialize($data['duration']);?>

<table border='1'>
        <tr>
            <td> ID </td>
            <td> title </td>
            <td> duration </td>
        </tr>
       <tr>
            <td> <?php echo $data['id'];?> </td>
            <td> <?php echo $data['title'];?> </td>
            <td> <?php echo $duration['month'].', '.$duration['hours'];?> </td>
        </tr>
</table>

