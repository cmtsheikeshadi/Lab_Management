<?php
include_once ('../../vendor/autoload.php');
use App\Courses\Courses;

$obj = new Courses();
$data = $obj->prepare($_REQUEST)->show();
//echo '<pre>';
//print_r($data);

?>
<?php
/**
 * Created by PhpStorm.
 * User: Mahbub-Alam
 * Date: 08-08-16
 * Time: 12.03
 */
include_once "links.php";
echo "<body>";
include_once "header.php";

//<!-- Page container -->
echo '<div class="page-container">

		<!-- Page content -->
		<div class="page-content">';

include_once "menu.php";
?>
<div class="content-wrapper">

    <!-- Page header -->
    <div class="page-header">
        <div class="page-header-content">
            <div class="page-title">
                <h4><a href="index.php"><i class="icon-arrow-left52 position-left"></i></a> <span class="text-semibold">Home</span> - </h4>
            </div>

            <div class="heading-elements">
                <div class="heading-btn-group">
                    <a href="#" class="btn btn-link btn-float has-text"><i class="icon-bars-alt text-primary"></i><span>Statistics</span></a>
                    <a href="#" class="btn btn-link btn-float has-text"><i class="icon-calculator text-primary"></i> <span>Invoices</span></a>
                    <a href="#" class="btn btn-link btn-float has-text"><i class="icon-calendar5 text-primary"></i> <span>Schedule</span></a>
                </div>
            </div>
        </div>

        <div class="breadcrumb-line">
            <ul class="breadcrumb">
                <li><a href="index.html"><i class="icon-home2 position-left"></i> Home</a></li>
                <li class="active">Course Setup</li>
            </ul>

            <ul class="breadcrumb-elements">
                <li><a href="#"><i class="icon-comment-discussion position-left"></i> Support</a></li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="icon-gear position-left"></i>
                        Settings
                        <span class="caret"></span>
                    </a>

                    <ul class="dropdown-menu dropdown-menu-right">
                        <li><a href="#"><i class="icon-user-lock"></i> Account security</a></li>
                        <li><a href="#"><i class="icon-statistics"></i> Analytics</a></li>
                        <li><a href="#"><i class="icon-accessibility"></i> Accessibility</a></li>
                        <li class="divider"></li>
                        <li><a href="#"><i class="icon-gear"></i> All settings</a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
    <!-- /page header -->


    <!-- Content area -->
    <div class="content">

        <div class="panel panel-flat">
            <div class="panel-heading">
                <h5 class="panel-title">Course Information</h5>
                <div class="heading-elements">
                    <ul class="icons-list">
                        <li><a data-action="collapse"></a></li>
                        <li><a data-action="reload"></a></li>
                        <li><a data-action="close"></a></li>
                    </ul>
                </div>
            </div>

            <div class="table-responsive">
                <table class="table">
                    <thead>
                    </thead>
                    <tbody>
                    <tr>
                        <th>Title</th>
                        <td><span style="color:green;font-size:22px;"><?php echo $data['title']; ?></span></td>
                    </tr>
                    <tr>
                        <th>Duration</th>
                        <td><?php  $duration = unserialize($data['duration']); echo $duration['month']." ".$duration['hours']; ?></td>
                    </tr>

                    <tr>
                        <th>Course Name</th>
                        <td><?php echo $data['title']; ?></td>
                    </tr>
                    <tr>
                        <th>Course Duration</th>
                        <td><?php echo $duration['month'].' '.$duration['hours'];; ?></td>
                    </tr>
                    <tr>
                        <th>Description</th>
                        <td><?php echo $data['description']; ?></td>
                    </tr>
                    <tr>
                        <th>Course Type</th>
                        <td><?php echo $data['course_type']; ?></td>
                    </tr>
                    <tr>
                        <th>Course Fee</th>
                        <td><?php echo $data['course_fee']; ?></td>
                    </tr>
                    <tr>
                        <th>Is Active Course</th>
                        <td><?php if(isset($data['is_active']) && $data['is_active'] == '1'){echo 'Running Course';} else { echo 'Course is not Running';}  ?></td>
                    </tr>
                    
                    </tbody>
                </table>
            </div>
        </div>
        <!-- Multiple rows select -->

        <!-- /multiple rows select -->



    </div>

</div>

<!-- Footer -->
<div class="footer text-muted">
    &copy; 2015. <a href="#">Limitless Web App Kit</a> by <a href="http://themeforest.net/user/Kopyov" target="_blank">Eugene Kopyov</a>
</div>
<!-- /footer -->

</div>
<!-- /content area -->

</div>
<?php echo '</div>

		<!-- Page content -->
		</div> </body> </html>';
?>


