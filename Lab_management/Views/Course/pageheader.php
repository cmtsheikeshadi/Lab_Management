   <!-- Page header -->
    <div class="page-header">
        <div class="page-header-content">
            <div class="page-title">
                <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Home</span> - Course Setup</h4>
            </div>

            <div class="heading-elements">
                <div class="heading-btn-group">
                    <a href="#" class="btn btn-link btn-float has-text"><i class="icon-bars-alt text-primary"></i><span>Statistics</span></a>
                    <a href="#" class="btn btn-link btn-float has-text"><i class="icon-calculator text-primary"></i> <span>Invoices</span></a>
                    <a href="#" class="btn btn-link btn-float has-text"><i class="icon-calendar5 text-primary"></i> <span>Schedule</span></a>
                </div>
            </div>
        </div>
        
        
        <div class="page-header-content" style="margin: 10px;">
            <button type="button" class="btn btn-default"> <a href="index.php">All Course List</a></button>
            <button type="button" class="btn btn-default"> <a href="create.php">Add New Course</a></button>
            <button type="button" class="btn btn-default"> <a href="restore_show.php">View Deleted List</a></button>
			<button type="button" class="btn btn-default"> <a href="counse_running.php">Counse Running List</a></button>
        </div>
        
        <div class="page-header-content">
        </div>
            
        
    </div>
    <!-- /page header -->
