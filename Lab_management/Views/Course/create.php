<?php
include_once "links.php";
echo "<body>";
include_once "header.php";

//<!-- Page container -->
echo '<div class="page-container">

		<!-- Page content -->
		<div class="page-content">';

include_once "menu.php"; ?>
<div class="content-wrapper">
<?php include 'pageheader.php';?>
    <!-- Page header -->
    <div class="page-header">
     
    <!-- Content area -->
    <div class="content">


        <!-- Multiple rows select -->
        <div class="panel panel-flat">
            <div class="panel-heading">
                <h5 class="panel-title">Course Lists</h5>
                <div class="heading-elements">
                    <ul class="icons-list">
                        <li><a data-action="collapse"></a></li>
                        <li><a data-action="reload"></a></li>
                        <li><a data-action="close"></a></li>
                    </ul>
                </div>
            </div>
            <form action="store.php" method="post">
                <div class="form-group">
                    <label>Course Title:</label>
                    <input type="text" class="form-control" name="title" placeholder="Course Title">
                </div>

                <div class="form-group">
                    <label>Course Type:</label>
                    <select name="course_type" id="select" class="select form-control">
                        <option value="Free Course"> Free Course </option>
                        <option value="Paid Course"> Paid Course </option>
                    </select>
                </div>

                <div class="form-group">

                    <span id="availability2"></span>


                </div>

                <div class="form-group">
                    <label>Course Duration In Month:</label>
                    <select name="duration_m" class="select form-control">
                        <option value="1 Month"> 1 Month</option>
                        <option value="2 Month"> 2 Month</option>
                        <option value="3 Month"> 3 month</option>
                        <option value="4 Month"> 4 Month</option>
                        <option value="5 Month"> 5 month</option>
                        <option value="6 Month"> 6 Month</option>
                    </select>
                </div>

                <div class="form-group">
                    <label>Course Duration In Houre:</label>
                    <input  type="text" name="duration_h" class="select form-control" placeholder="144 Hours">
                </div>

                <div class="form-group">
                    <label class="text-semibold">Is Active?</label>
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="is_active" checked="checked">
                            Checked default
                        </label>
                    </div>
                </div>

                <div class="form-group">
                    <label class="text-semibold">Description</label>
                    <div>
                        <textarea cols="18" rows="18" class="wysihtml5 wysihtml5-default form-control" placeholder="Enter text ...">
                        </textarea>
                    </div>
                </div>

<div class="form-group">
    <div class="row">
        <div class="col-md-2"></div>
        <div class="col-md-8">
            <input type="submit" class="btn btn-info btn-block center" name="submit" value="Submit Button">
        </div>
        <div class="col-md-2"></div>
    </div>
</div>
        </div>
    </form>
        </div>

        <!-- /multiple rows select -->


    &copy; 2015. <a href="#">Limitless Web App Kit</a> by <a href="http://themeforest.net/user/Kopyov" target="_blank">Eugene Kopyov</a>
    </div>
<!-- Footer -->



<!-- /footer -->

<?php echo '</div>

		<!-- Page content -->
		</div> </body> </html>';
?>
