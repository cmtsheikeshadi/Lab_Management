<?php
include_once ('../../vendor/autoload.php');
use App\Courses\Courses;

//print_r($_POST);
//die();

$obj = new Courses();
$data = $obj->filterbypay($_POST['course_type']);
//echo '<pre>';
//print_r($data);
//die();
?>
<table class="table datatable-tools-select-multiple" id="dt">
    <thead>
    <tr>
        <th>Title</th>
        <th>Course Type</th>
        <th>Course Fee</th>
        <th>Description</th>
        <th>Duration</th>
        <th>Is Active</th>
        <th class="text-center">Actions</th>
    </tr>
    </thead>
    <tbody>
    <?php if(isset($data)){?>
        <?php  foreach ($data as $item):?>
            <?php //$duration = unserialize($item['duration']);?>

            <tr>
                <td><?php echo $item['title']; ?></td>
                <td><?php echo $item['course_type']; ?></td>
                <td><?php echo $item['course_fee']; ?></td>
                <td><?php echo $item['description']; ?></td>
                <td><?php //echo $duration['month']." ".$duration['hours']; ?></td>
                <td><td><?php if(isset($item['is_active']) && $item['is_active'] == '1'){echo 'Running Course';} else { echo '<p style="color:red;">Course is not Running</p>';}  ?></td>
                        </td>
                <td>
                    <ul class="icons-list">
                        <li class="text-primary-600"><a href="edit.php?id=<?php echo $item['unique_id']; ?>"><i class="icon-pencil7"></i></a></li>
                        <li class="text-danger-600"><a href="delete.php?id=<?php echo $item['unique_id']; ?>"><i class="icon-trash"></i></a></li>
                        <li class="text-teal-600"><a href="show.php?id=<?php echo $item['unique_id']; ?>"><i class="icon-three-bars"></i></a></li>
                        <li class="text-primary-600"><a href="#"><i class="icon-detail"></i></a></li>
                    </ul>

                </td>
            </tr>
        <?php endforeach;?>
    <?php }else { ?>
        <tr>
            <td colspan="6"> <p style='color:red;'> No Data</p></td>
        </tr>
    <?php }?>
    </tbody>
</table>

