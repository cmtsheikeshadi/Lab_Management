<?php
namespace App\CourseTraLabMapp;
use PDO;

class CourseTraLabMapp{
    public $id = "";
    public $course_id = "";
    public $course_name = "";
    public $lead_trainer = "";
    public $asst_trainer = "";
    public $lab_asst = "";
    public $lab_id = "";
    public $batch_no = "";
    public $start_date = "";
    public $ending_date = "";
    public $start_time = "";
    public $ending_time = "";
    public $day = "";
    public $assigned_by = "";
    public $time = "";
    public $conn = "";


    public function __construct() {
        session_start();
        $this->conn = new PDO('mysql:dbname=lab;host=localhost', 'root', '');
    }

    public function prepare($data= ""){
        
        if(!empty($data['id'])){
            $this->id = $data['id'];
        }
        
        if(!empty($data['course_id'])){
            $this->course_id = $data['course_id'];
        }
        
         if(!empty($data['course_name'])){
            $this->course_name = $data['course_name'];
        }
        
        if(!empty($data['lead_trainer'])){
            $this->lead_trainer = $data['lead_trainer'];
        }
        
        if(!empty($data['asst_trainer'])){
            $this->asst_trainer = $data['asst_trainer'];
        }
        
        if(!empty($data['lab_asst'])){
            $this->lab_asst = $data['lab_asst'];
        }
        
        if(!empty($data['lab_id'])){
            $this->lab_id = $data['lab_id'];
        }
        
        if(!empty($data['batch_no'])){
            $this->batch_no = $data['batch_no'];
        }
        
        if(!empty($data['start_date'])){
            $this->start_date = $data['start_date'];
        }
        
        if(!empty($data['ending_date'])){
            $this->ending_date = $data['ending_date'];
        }
        
        if(!empty($data['start_time'])){
            $this->start_time = $data['start_time'];
        }
        
        if(!empty($data['ending_time'])){
            $this->ending_time = $data['ending_time'];
        }
        
        if(!empty($data['day'])){
            $this->day = $data['day'];
        }
        
        if(!empty($data['assigned_by'])){
            $this->assigned_by = $data['assigned_by'];
        }
        
        return $this;
       // print_r($data);
    }
    
    public function store(){
         
       try{
            date_default_timezone_set("Asia/Dhaka"); 
            $this->time = date('Y-m-d H:i:s'); //Returns IST 
            
            $query = "INSERT INTO `lab`.`course_trainer_lab_mapping` (`course_id`, `batch_no`, `lead_trainer`, `asst_trainer`, `lab_asst`, `lab_id`, `start_date`, `ending_date`, `start_time`, `ending_time`, `day`, `assigned_by`, `created`) 
                      VALUES (:course_id, :batch_no, :lead_trainer, :asst_trainer, :lab_asst, :lab_id, :start_date, :ending_date, :start_time, :ending_time, :day, :assigned_by, :created)";
                
            $result = $this->conn->prepare($query);
                $result->execute(array(
                    "course_id" => $this->course_id,
                    "batch_no" => $this->batch_no,
                    "lead_trainer" => $this->lead_trainer,
                    "asst_trainer" => $this->asst_trainer,
                    "lab_asst" => $this->lab_asst,
                    "lab_id" => $this->lab_id,
                    "start_date" => $this->start_date,
                    "ending_date" => $this->ending_date,
                    "start_time" => $this->start_time,
                    "ending_time" => $this->ending_time,
                    "day" => $this->day,
                    "assigned_by" => $this->assigned_by,
                    "created" => $this->time,
                ));
                
                $_SESSION['Message'] = 'Data add Successfully';
                header('location: create.php');
                
            } catch (PDOException $e){
                echo"Error: " .$e->getMassage();
            }
    }
    
    public function index(){
        try{
            $query = "SELECT 
                        courses.title,
                        course_trainer_lab_mapping.id,
                        course_trainer_lab_mapping.batch_no,
                        course_trainer_lab_mapping.lead_trainer,
                        course_trainer_lab_mapping.start_date,
                        course_trainer_lab_mapping.ending_date,
                        course_trainer_lab_mapping.start_time,
                        course_trainer_lab_mapping.ending_time,
                        course_trainer_lab_mapping.day,
                        labinfo.lab_no
                      From courses,course_trainer_lab_mapping,labinfo
                        where courses.id = course_trainer_lab_mapping.course_id 
                        and course_trainer_lab_mapping.course_id = labinfo.course_id";
            
            $result = $this->conn->query($query) or die("Index query not ok");
            while ($row = $result->fetch(PDO::FETCH_ASSOC)){
                    $data[] = $row;
                }
            } catch (PDOException $e){
            echo"Error: " .$e->getMassage();
            }
            if(isset($data)){return $data;}
    }
    
    public function show(){
        try{
            $query = "SELECT 
                        courses.title,
                        course_trainer_lab_mapping.id,
                        course_trainer_lab_mapping.batch_no,
                        course_trainer_lab_mapping.lead_trainer,
                        course_trainer_lab_mapping.start_date,
                        course_trainer_lab_mapping.ending_date,
                        course_trainer_lab_mapping.start_time,
                        course_trainer_lab_mapping.ending_time,
                        course_trainer_lab_mapping.day,
                        labinfo.lab_no
                      From courses,course_trainer_lab_mapping,labinfo
                        where courses.id = course_trainer_lab_mapping.course_id 
                        and course_trainer_lab_mapping.course_id = labinfo.course_id";
            $result = $this->conn->query($query);
            $row = $result->fetch(PDO::FETCH_ASSOC);
            return $row; 
        }catch (PDOException $e){
            echo"Error: " .$e->getMassage();
        }
    }
   
    public function delete(){
        try{
            $query = "DELETE FROM `lab`.`course_trainer_lab_mapping` WHERE `course_trainer_lab_mapping`.`id` = '$this->id'";
            $this->conn->query($query);
            $_SESSION['Message'] = "Data has been Delete";
            header('location:index.php');
        }catch (PDOException $e){
            echo"Error: " .$e->getMassage();
        }
    }
    
     public function update(){
       try  {
                $ans = array( 'month'=>$this->duration_m,'hours'=>$this->duration_h);
                $this->duration = serialize($ans); 

                date_default_timezone_set("Asia/Dhaka"); 
                $this->time = date('Y-m-d H:i:s'); //Returns IST 
                
               $query = "UPDATE `lab`.`courses` SET `title` = '$this->title', `duration` = '$this->duration', `description` = '$this->description', `course_type` = '$this->course_type', `course_fee` = '$this->course_fee', `updated` = '$this->time' WHERE `courses`.`unique_id` = '$this->id';";
              
               echo $query;
               die();
               $result = $this->conn->query($query);
               $result->execute(array(':title'=>$this->title, ':duration'=>$this->duration, ':description'=>$this->description, ':course_type'=>$this->course_type, ':course_fee'=>$this->course_fee, ':updated'=>$this->time,));
               $_SESSION['Message'] = "Data has been Update";
               header('location:index.php'); 
            }
            catch(PDOException $e) {
              echo $e->getMessage();
            }
      }  
     
    public function trashed(){
         date_default_timezone_set("Asia/Dhaka"); 
         $this->time = date('Y-m-d H:i:s'); //Returns IST 
        
        $query = "UPDATE `lab`.`courses` SET `is_active` = '0', `deleted` = '$this->time' WHERE `courses`.`unique_id` = '$this->id';";
        $this->conn->query($query);
        $_SESSION['Message'] = "Data has been Trashed";
        header('location:index.php');
    }

    public function trashed_show(){
         try{
            $query = "SELECT * FROM `courses` WHERE `is_active`='0' ORDER BY `deleted` DESC ";
            $result = $this->conn->query($query) or die("Index query not ok");
            while ($row = $result->fetch(PDO::FETCH_ASSOC)){
                    $data[] = $row;
                }
            } catch (PDOException $e){
            echo"Error: " .$e->getMassage();
            }
            return $data;
    }
   
    
    public function restore(){
        $query = "UPDATE `lab`.`courses` SET `is_active` = '1' WHERE `courses`.`unique_id` = '$this->id';";
        $this->conn->query($query);
        $_SESSION['Message'] = "Data has been Trashed";
        header('location:index.php');    
    }
    
 
    
   public function courses_show(){
        try{
            $query = "SELECT * FROM `courses` WHERE `id`='$this->id'";
            $result = $this->conn->query($query);
            $row = $result->fetch(PDO::FETCH_ASSOC);
            return $row; 
        }catch (PDOException $e){
            echo"Error: " .$e->getMassage();
        }
    }
    
    public function trainers_show(){
        try{
            $query = "SELECT * FROM `trainers` WHERE `course_name`='$this->course_name'";
            $result = $this->conn->query($query);
            while($course_name_list = $result->fetch(PDO::FETCH_ASSOC)){
                       $lt_trainer[]= $course_name_list;
            }
            if(isset($lt_trainer)){return $lt_trainer;}
        }catch (PDOException $e){
            echo"Error: " .$e->getMassage();
        }
    }
    
    public function labinfo_show(){
        try{
            $query = "SELECT * FROM `labinfo` WHERE `course_id`='$this->id'";
            $result = $this->conn->query($query);
            while($lab_info = $result->fetch(PDO::FETCH_ASSOC)){
                       $lab_no[]= $lab_info;
            }
            if(isset($lab_no)){return $lab_no;}
        }catch (PDOException $e){
            echo"Error: " .$e->getMassage();
        }
    }
    
    public function valide(){
        try{
            $query = "SELECT * FROM `course_trainer_lab_mapping` WHERE `lab_id` = '$this->lab_id' && `start_date`='$this->start_date' && `ending_date`='$this->ending_date' && `start_time` = '$this->start_time' && `ending_time`= '$this->ending_time' && `day`='$this->day'";
            echo $query;
            die();
            $result = $this->conn->query($query);
            
            while($valide_info = $result->fetch(PDO::FETCH_ASSOC)){
                $valide[]= $valide_info;
            }
            if(isset($valide)){return $valide;}
        }catch (PDOException $e){
            echo"Error: " .$e->getMassage();
        }
    }
    
    
}


?>
