-- phpMyAdmin SQL Dump
-- version 4.1.6
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Aug 13, 2016 at 05:31 AM
-- Server version: 5.6.16
-- PHP Version: 5.5.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `lab`
--

-- --------------------------------------------------------

--
-- Table structure for table `courses`
--

CREATE TABLE IF NOT EXISTS `courses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `unique_id` varchar(111) NOT NULL,
  `title` varchar(111) NOT NULL,
  `duration` varchar(255) NOT NULL,
  `description` varchar(500) NOT NULL,
  `course_type` varchar(50) NOT NULL,
  `course_fee` varchar(111) NOT NULL,
  `is_active` int(10) NOT NULL,
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL,
  `deleted` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=32 ;

--
-- Dumping data for table `courses`
--

INSERT INTO `courses` (`id`, `unique_id`, `title`, `duration`, `description`, `course_type`, `course_fee`, `is_active`, `created`, `updated`, `deleted`) VALUES
(25, '57ab30c815698', 'Web App Develop- PHP', 'a:2:{s:5:"month";s:7:"3 Month";s:5:"hours";s:9:"144 Hours";}', 'PHP, HTML, CSS, Javascript, OOP', 'Free Course', '', 1, '2016-08-10 19:48:56', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(26, '57ab30ea9eeff', 'Digital Marketing Course', 'a:2:{s:5:"month";s:7:"2 Month";s:5:"hours";s:9:"128 Hours";}', 'Mardeting Section', 'Free Course', '', 1, '2016-08-10 19:49:30', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(27, '57ab311f22fd3', 'Mobile App Develop', 'a:2:{s:5:"month";s:7:"3 Month";s:5:"hours";s:9:"144 Hours";}', 'Mobile application', 'Free Course', '', 1, '2016-08-10 19:50:23', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(28, '57ab313d580cf', 'SEO', 'a:2:{s:5:"month";s:7:"1 Month";s:5:"hours";s:8:"48 Hours";}', 'Search Engine Optimization', 'Free Course', '', 0, '2016-08-10 19:50:53', '0000-00-00 00:00:00', '2016-08-12 10:47:02'),
(29, '57ab316742637', 'Web Design', 'a:2:{s:5:"month";s:7:"6 Month";s:5:"hours";s:9:"144 Hours";}', 'PHP, HTML, CSS, Javascript, OOP', 'Free Course', '', 1, '2016-08-10 19:51:35', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(30, '57ab31b76c887', 'Graphics And Web design', 'a:2:{s:5:"month";s:7:"3 Month";s:5:"hours";s:9:"128 Hours";}', 'Photoshop, Illustrator, Auto Cat ', 'Paid Course', '5000', 1, '2016-08-10 19:52:55', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(31, '57ac2f9a4ac33', 'Digital Marketing Course', 'a:2:{s:5:"month";s:7:"1 Month";s:5:"hours";s:9:"128 Hours";}', 'afj', 'Free Course', '', 0, '2016-08-11 13:56:10', '0000-00-00 00:00:00', '2016-08-12 10:46:33');

-- --------------------------------------------------------

--
-- Table structure for table `course_trainer_lab_mapping`
--

CREATE TABLE IF NOT EXISTS `course_trainer_lab_mapping` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `course_id` int(111) NOT NULL,
  `batch_no` varchar(111) NOT NULL,
  `lead_trainer` varchar(111) NOT NULL,
  `asst_trainer` varchar(111) NOT NULL,
  `lab_asst` varchar(111) NOT NULL,
  `lab_id` int(111) NOT NULL,
  `start_date` varchar(111) NOT NULL,
  `ending_date` varchar(111) NOT NULL,
  `start_time` varchar(111) NOT NULL,
  `ending_time` varchar(111) NOT NULL,
  `day` varchar(111) NOT NULL,
  `assigned_by` varchar(255) NOT NULL,
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL,
  `deleted` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `course_trainer_lab_mapping`
--

INSERT INTO `course_trainer_lab_mapping` (`id`, `course_id`, `batch_no`, `lead_trainer`, `asst_trainer`, `lab_asst`, `lab_id`, `start_date`, `ending_date`, `start_time`, `ending_time`, `day`, `assigned_by`, `created`, `updated`, `deleted`) VALUES
(3, 25, '27', 'Mango', 'Pot', 'lichu', 1, '2016-01-07', '2016-01-08', '09:00', '12:00', 'Friday', 'Shadi', '2016-08-10 23:46:20', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(5, 25, '27', 'Mango', 'Pot', 'lichu', 1, '2016-01-07', '2016-01-08', '13:30', '17:30', 'Satur-Mon-Wednes', 'Shadi', '2016-08-10 23:57:56', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(6, 25, '3', 'Mango', 'Pot', 'lichu', 301, '2016-05-07', '2016-29-07', '17:30', '21:00', 'Sun-Tues-Thurs', 'Shadi', '2016-08-10 23:59:55', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(8, 28, '3', 'tatul', 'kutu kutu', '', 3, '2016-08-01', '2016-08-30', '13:30', '17:30', 'Sun-Tues-Thurs', 'shadi', '2016-08-11 09:54:49', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(9, 25, '5', 'Mango', 'Pot', 'lichu', 1, '2016-08-01', '2016-08-16', '09:00', '17:30', 'Friday', 'shadi', '2016-08-11 09:56:58', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(10, 28, '50', 'tatul', 'kutu kutu', '', 402, '2016-08-12', '2016-08-18', '09:00', '13:00', 'Satur-Mon-Wednes', '', '2016-08-11 14:25:16', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `installed_softwares`
--

CREATE TABLE IF NOT EXISTS `installed_softwares` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `labinfo_id` int(111) NOT NULL,
  `software_title` varchar(111) NOT NULL,
  `version` varchar(111) NOT NULL,
  `software_type` varchar(111) NOT NULL,
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL,
  `deleted` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `labinfo`
--

CREATE TABLE IF NOT EXISTS `labinfo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `course_id` int(100) NOT NULL,
  `lab_no` varchar(111) NOT NULL,
  `seat_capacity` varchar(111) NOT NULL,
  `projector_resolution` varchar(111) NOT NULL,
  `ac_status` varchar(111) NOT NULL,
  `pc_configuration` varchar(255) NOT NULL,
  `os` varchar(255) NOT NULL,
  `trainer_pc_configuration` varchar(255) NOT NULL,
  `table_capacity` varchar(100) NOT NULL,
  `internet_speed` varchar(100) NOT NULL,
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL,
  `deleted` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `labinfo`
--

INSERT INTO `labinfo` (`id`, `course_id`, `lab_no`, `seat_capacity`, `projector_resolution`, `ac_status`, `pc_configuration`, `os`, `trainer_pc_configuration`, `table_capacity`, `internet_speed`, `created`, `updated`, `deleted`) VALUES
(1, 25, '301', '', '', '', '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 24, '401', '', '', '', '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 28, '402', '', '', '', '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, 26, '402', '', '', '', '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(5, 27, '302', '', '', '', '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(7, 24, '302', '', '', '', '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(8, 25, '304', '3', '1000x800', '', '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `trainers`
--

CREATE TABLE IF NOT EXISTS `trainers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `unique_id` varchar(111) NOT NULL,
  `full_name` varchar(111) NOT NULL,
  `edu_status` varchar(255) NOT NULL,
  `team` varchar(111) NOT NULL,
  `course_name` varchar(111) NOT NULL,
  `trainer_level` varchar(111) NOT NULL,
  `image` varchar(255) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `email` varchar(111) NOT NULL,
  `address` varchar(255) NOT NULL,
  `gender` varchar(20) NOT NULL,
  `web` varchar(111) NOT NULL,
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL,
  `deleted` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `trainers`
--

INSERT INTO `trainers` (`id`, `unique_id`, `full_name`, `edu_status`, `team`, `course_name`, `trainer_level`, `image`, `phone`, `email`, `address`, `gender`, `web`, `created`, `updated`, `deleted`) VALUES
(1, '', 'Mango', 'Bsc', '', 'Web App Develop- PHP', 'Led Trainer', '', '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, '', 'Pot', 'Bsc', '', 'Web App Develop- PHP', 'Assistant Trainer', '', '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, '', 'lichu', 'Hsc', '', 'Web App Develop- PHP', 'Lab Assistant', '', '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, '', 'tatul', 'ssc', '', 'SEO', 'Led Trainer', '', '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(5, '', 'kutu kutu', 'ssc', '', 'SEO', 'Assistant Trainer', '', '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(6, '', 'ja kisu', 'sdk', '', 'Digital Marketing Course', 'Assistant Trainer', '', '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(7, '', 'ki dibo', '', '', 'Digital Marketing Course', 'Led Trainer', '', '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `unique_id` varchar(111) NOT NULL,
  `full_name` varchar(111) NOT NULL,
  `username` varchar(50) NOT NULL,
  `email` varchar(111) NOT NULL,
  `password` varchar(20) NOT NULL,
  `image` varchar(255) NOT NULL,
  `is_active` int(10) NOT NULL,
  `is_admin` int(10) NOT NULL,
  `is_delete` int(10) NOT NULL,
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL,
  `deleted` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
